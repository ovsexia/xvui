import { createVNode, render } from 'vue';
import Album from './xvAlbum.vue';
import createEle from '../../tools/createEle.js';

export default (options) => {
  const outer = createEle(options.zindex || Album.props.zindex.default, 'xvalbum_box');

  return new Promise((reslove, reject) => {
    const submitCallBack = () => {
      render(null, outer)
      outer.remove();
      reslove(true);
    };

    const cancelCallBack= () => {
      render(null, outer);
      //outer.remove();
      //reject();
    };

    const VNode = createVNode(Album, {...options, submitCallBack, cancelCallBack});
    render(VNode, outer);
  });
};