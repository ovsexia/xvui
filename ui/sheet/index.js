import { createVNode, render } from 'vue';
import Sheet from './xvSheet.vue';
import createEle from '../../tools/createEle.js';

export default (options) => {
  const outer = createEle(options.zindex || Sheet.props.zindex.default, 'xvsheet_box');

  return new Promise((reslove, reject) => {
    const submitCallBack = () => {
      render(null, outer)
      outer.remove();
      reslove(true);
    };

    const cancelCallBack= () => {
      render(null, outer);
      //outer.remove();
      //reject();
    };

    const VNode = createVNode(Sheet, {...options, submitCallBack, cancelCallBack});
    render(VNode, outer);
  });
};