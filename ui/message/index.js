import { createVNode, render } from 'vue';
import Message from './xvMessage.vue';
import createEle from '../../tools/createEle.js';

export default (options) => {
  const outer = createEle(options.zindex || Message.props.zindex.default);

  return new Promise((reslove, reject) => {
    const submitCallBack = () => {
      render(null, outer)
      outer.remove();
      reslove(true);
    };

    const cancelCallBack= () => {
      render(null, outer);
      outer.remove();
      //reject();
    };

    const VNode = createVNode(Message, {...options, submitCallBack, cancelCallBack});
    render(VNode, outer);
  });
};