import xvBr from './ui/br';
import xvHr from './ui/hr';
import xvCell from './ui/cell';
import xvNodata from './ui/nodata';
import { xvRows, xvRow } from './ui/row';
import xvInput from './ui/input';
import xvSelect from './ui/select';
import xvAffix from './ui/affix';
import xvThide from './ui/thide';
import xvLoading from './ui/loading';
import xvMessage from './ui/message';
import xvSheet from './ui/sheet';
import xvAlbum from './ui/album';
import xvVerify from './tools/verify.js';
import xvDateFormat from './tools/dateFormat.js';
import xvActcall from './tools/actCall.js';

const components = [
  ['xvBr', xvBr],
  ['xvHr', xvHr],
  ['xvCell', xvCell],
  ['xvNodata', xvNodata],
  ['xvRows', xvRows],
  ['xvRow', xvRow],
  ['xvInput', xvInput],
  ['xvSelect', xvSelect],
  ['xvAffix', xvAffix],
  ['xvThide', xvThide],
  ['xvLoading', xvLoading],
];

export {
  xvBr,
  xvHr,
  xvCell,
  xvNodata,
  xvRows,
  xvRow,
  xvInput,
  xvSelect,
  xvMessage,
  xvSheet,
  xvAlbum,
  xvVerify,
  xvDateFormat,
  xvActcall,
};

export default {
  version: '1.2.11',
  install: (app, options) => {
    components.forEach((item) => {
      app.component(item[0], item[1]);
    });
  },
};