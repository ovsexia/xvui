/* 时间戳转换
 * date    时间戳
 * format  格式。例如 Y/m/d H:i:s
 */
const dateFormat = (date, format='Y-m-d', lang='en')=> {
  if(!date){
    return '';
  }
  if(format=='parse'){  //转时间戳
    return Date.parse(date);
  }

  if(date.toString().length==10){
    date = Number(date + '000');
  }
  let d = new Date(date);
  let arr = format.split('');
  let str = '';
  let dstr = '';
  for(let i=0; i<arr.length; i++){
    if(arr[i]=='y'){
      str += d.getFullYear().toString().slice(2);
    }else if(arr[i]=='Y'){
      str += d.getFullYear();
    }else if(arr[i]=='m'){
      dstr = (d.getMonth() + 1).toString();
      str += (dstr.length==1 ? '0' : '') + dstr;
    }else if(arr[i]=='n'){
      str += d.getMonth() + 1;
    }else if(arr[i]=='F'){
      str += monthText(d.getMonth() + 1, (lang=='cn' || lang=='jp' ? lang : 'en'));
    }else if(arr[i]=='M'){
      str += monthText(d.getMonth() + 1, (lang=='cn' || lang=='jp' ? lang : 'en_min'));
    }else if(arr[i]=='d'){
      dstr = d.getDate().toString();
      str += (dstr.length==1 ? '0' : '') + dstr;
    }else if(arr[i]=='j'){
      str += d.getDate();
    }else if(arr[i]=='H'){
      dstr = d.getHours().toString();
      str += (dstr.length==1 ? '0' : '') + dstr;
    }else if(arr[i]=='h'){
      let darr_h = time12(d.getHours());
      dstr = darr_h[0].toString();
      str += (dstr.length==1 ? '0' : '') + dstr;
    }else if(arr[i]=='A' || arr[i]=='a'){
      let darr_a = time12(d.getHours());
      dstr = darr_a[1].toString();
      if(arr[i]=='A'){
        dstr = dstr.toUpperCase();
      }
      str += dstr;
    }else if(arr[i]=='i'){
      dstr = d.getMinutes().toString();
      str += (dstr.length==1 ? '0' : '') + dstr;
    }else if(arr[i]=='s'){
      dstr = d.getSeconds().toString();
      str += (dstr.length==1 ? '0' : '') + dstr;
    }else if(arr[i]=='S'){
      dstr = d.getDate();
      if(dstr==1){
        str += 'st';
      }else if(dstr==2){
        str += 'nd';
      }else if(dstr==3){
        str += 'rd';
      }else{
        str += 'th';
      }
    }else if(arr[i]=='w'){
      str += d.getDay();
    }else if(arr[i]=='N'){
      dstr = d.getDay();
      str += dstr==0 ? 7 : dstr;
    }else if(arr[i]=='l'){
      str += weekText(d.getDay(), (lang=='cn' || lang=='jp' ? lang : 'en'));
    }else if(arr[i]=='D'){
      str += weekText(d.getDay(), (lang=='cn' || lang=='jp' ? lang : 'en_min'));
    }else{
      str += arr[i];
    }
  }
  return str;
}

//12小时制
const time12 = (times)=> {
  if(times <= 12){
    return [times, 'am'];
  }else if(times > 12 && times<=24){
    return [times - 12, 'pm'];
  }
}

//月份数字转文本
const monthText = (month, type)=> {
  const textList = {
    'en': {
      1: 'January',
      2: 'February',
      3: 'March',
      4: 'April',
      5: 'May',
      6: 'June',
      7: 'July',
      8: 'August',
      9: 'September',
      10: 'October',
      11: 'November',
      12: 'December',
    },
    'en_min': {
      1: 'Jan',
      2: 'Feb',
      3: 'Mar',
      4: 'Apr',
      5: 'May',
      6: 'Jun',
      7: 'Jul',
      8: 'Aug',
      9: 'Sep',
      10: 'Oct',
      11: 'Nov',
      12: 'Dec',
    },
    'cn': {
      1: '一月',
      2: '二月',
      3: '三月',
      4: '四月',
      5: '五月',
      6: '六月',
      7: '七月',
      8: '八月',
      9: '九月',
      10: '十月',
      11: '十一月',
      12: '十二月',
    },
  }
  return textList[type][month];
}

//星期数字转文本
const weekText = (week, type)=> {
  const textList = {
    'en': {
      0: 'Sunday',
      1: 'Monday',
      2: 'Tuesday',
      3: 'Wednesday',
      4: 'Thursday',
      5: 'Friday',
      6: 'Saturday',
    },
    'en_min': {
      0: 'Sun',
      1: 'Mon',
      2: 'Tue',
      3: 'Wed',
      4: 'Thu',
      5: 'Fri',
      6: 'Sat',
    },
    'cn': {
      0: '星期日',
      1: '星期一',
      2: '星期二',
      3: '星期三',
      4: '星期四',
      5: '星期五',
      6: '星期六',
    },
  }
  return textList[type][week];
}

export default dateFormat;