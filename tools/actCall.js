import { useEventListener } from '@vueuse/core';
import { effectScope } from 'vue';
import { throttle } from 'lodash-unified';

const scopeEventListener = effectScope();
const actCall = (option)=> {
  //默认参数
  const config = {
    call: null,
    click: null,
    touch: null,
    touchTime: 500,  //触摸响应时间
  }

  const C = {...config, ...option}

  if(!C.ele){
    return false;
  }

  const actCallStart = (type)=> {
    let fun = ()=> {};
    if(type=='click'){
      fun = C.click ? C.click : C.call;
    }else if(type=='touch'){
      fun = C.touch ? C.touch : C.call;
    }
    if(fun && typeof(fun)=='function'){
      fun();
    }
  }

  //阻止默认右键菜单
  const stopProp = (addtime=0)=> {
    const scopeEventListener_document = effectScope();
    const contextmenuHandler = throttle((e) => {
      e.preventDefault();
      return false;
    });

    const selectstartHandler = throttle((e) => {
      e.preventDefault();
      return false;
    });

    scopeEventListener_document.run(() => {
      useEventListener(document, 'contextmenu', contextmenuHandler);
      if(addtime){
        useEventListener(document, 'selectstart', selectstartHandler);
      }
    });

    setTimeout(()=> {
      scopeEventListener_document.stop();
    }, addtime + 100);
  }

  //右键
  const mousedownHandler = throttle((e) => {
    stopProp();
    if(e.button==2){
      actCallStart('click');
    }
  });

  //长按
  const touchstartHandler = throttle((e) => {
    const timer = setTimeout(()=> {
      stopProp(C.touchTime);
      actCallStart('touch');
    }, C.touchTime);
    const scopeEventListener_app = effectScope();

    const touchendHandler = throttle((e) => {
      clearTimeout(timer);
    });

    scopeEventListener_app.run(() => {
      useEventListener(C.ele, 'touchend', touchendHandler);
    });
  });

  scopeEventListener.run(() => {
    if(C.ele){
      useEventListener(C.ele, 'mousedown', mousedownHandler);
      useEventListener(C.ele, 'touchstart', touchstartHandler);
    }
  });
}

export default actCall;