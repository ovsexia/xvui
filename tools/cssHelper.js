function cssHelper(arr)
{
  let style = '';
  arr.forEach((item) => {
    item[0] = item[0].trim();
    if(item[0]=='transform'){
      style += item[0] + ': ' + item[1] + '(' + (item[2] || '') + '); ';
    }else{
      style += item[0] + ': ' + item[1] + (item[2] || '') + '; ';
    }
  });
  return style;
}

export default cssHelper