const verify = {};

//判断是否为空
verify['required'] = (val) => {
  if(typeof(val)=='undefined' || val==''){
    return false;
  }
  return true;
}

//判断是否为邮箱
verify['email'] = (val) => {
  const reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
  if(typeof(val)=='undefined' || val==''){
    return true;
  }else{
    if(reg.test(val)){
      return true;
    }else{
      return false;
    }
  }
}

//判断是否为数字，包含小数
verify['number'] = (val) => {
  const reg = /^[+]{0,1}(\d+)$|^[+]{0,1}(\d+\.\d+)$/;
  if(typeof(val)=='undefined' || val==''){
    return true;
  }else{
    if(reg.test(val)){
      return true;
    }else{
      return false;
    }
  }
}

//判断是否为整数
verify['int'] = (val) => {
  const reg = /^\d+$/;
  if(typeof(val)=='undefined' || val==''){
    return true;
  }else{
    if(reg.test(val)){
      return true;
    }else{
      return false;
    }
  }
}

//密码强度1，至少6位数
verify['pass1'] = (val) => {
  const reg = /\w{6}/;
  if(typeof(val)=='undefined' || val==''){
    return true;
  }else{
    if(reg.test(val)){
      return true;
    }else{
      return false;
    }
  }
}

//密码强度2，至少6位数+数字+字母
verify['pass2'] = (val) => {
  const reg = /^.*(?=.{6,})(?=.*\d)(?=.*[a-z]).*$/;
  if(typeof(val)=='undefined' || val==''){
    return true;
  }else{
    if(reg.test(val)){
      return true;
    }else{
      return false;
    }
  }
}

//密码强度3，至少6位数+数字+字母+至少一位大写字母
verify['pass3'] = (val) => {
  const reg = /^.*(?=.{6,})(?=.*\d)(?=.*[A-Z])(?=.*[a-z]).*$/;
  if(typeof(val)=='undefined' || val==''){
    return true;
  }else{
    if(reg.test(val)){
      return true;
    }else{
      return false;
    }
  }
}

//密码强度4，至少6位数+数字+字母+至少一位大写字母+至少一个特殊符号【!@#$%^&*?()+-】
verify['pass4'] = (val) => {
  const reg = /^.*(?=.{6,})(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*?()+-]).*$/;
  if(typeof(val)=='undefined' || val==''){
    return true;
  }else{
    if(reg.test(val)){
      return true;
    }else{
      return false;
    }
  }
}

//验证
verify['verify'] = (rules, value) => {
  let passed = true;
  let msg = '';
  Object.keys(rules).forEach(function(key, index){
    for(let i=0; i<rules[key].length; i++){
      if(!verify[rules[key][i].type](value[index])){
        passed = false;
        if(msg==''){
          msg = rules[key][i].msg;
        }
      }
    }
  });
  return {passed: passed, msg: msg};
};

export default verify;