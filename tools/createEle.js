function createEle(zindex, eleid)
{
  let outer = null;
  let h = false;
  if(eleid){
    outer = document.getElementById(eleid);
    if(outer){
      h = true;
    }else{
      outer = document.createElement('div');
      outer.setAttribute('id', eleid);
    }
  }else{
    outer = document.createElement('div');
  }

  if(zindex){
    outer.setAttribute('style', 'position: relative; z-index: ' + zindex + ';');
  }
  const button = document.createElement('button');
  button.setAttribute('style', 'opacity: 0; width: 0; height: 0; overflow: hidden; position: fixed; top: 0; left: 0; z-index: -100;');
  outer.appendChild(button);
  if(!h){
    document.body.appendChild(outer);
  }
  //按钮取消焦点防止重复触发
  button.focus();
  button.blur();
  button.remove();
  return outer;
}

export default createEle;