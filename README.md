# Xvui

#### 介绍
Xvui 是一款基于 vue3 开发的 ui 插件。

#### 预览地址
[http://xvui.ovsexia.com](http://xvui.ovsexia.com)

#### 从 npm 安装
`npm i @ovsexia/xvui`